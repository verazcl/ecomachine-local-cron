import os
import requests
import json
import subprocess
from dotenv import load_dotenv

load_dotenv()

API_URL = os.getenv("API_URL")
API_TOKEN = os.getenv("API_TOKEN")
RAM_DIR = os.getenv("RAM_DIR")
CPU_INFO = os.getenv("CPU_INFO")
MACHINE_CODE = os.getenv("MACHINE_CODE")

try:
    data = subprocess.run(CPU_INFO, shell=True, check=True, capture_output=True)
    MACHINE_CODE = str(data.stdout.decode("utf-8"))
except Exception as error:
    print("MACHINE_CODE error: ", error)

HEADERS = {"Content-Type": "application/json", "cache-control": "no-cache"}
r = requests.get(API_URL + "/machines/options/" + MACHINE_CODE, headers=HEADERS)

response = r.text
response = json.loads(response)

for data in response["data"]:
    file = data["file"]
    contents = data["data"]
    with open(os.path.join(RAM_DIR, file), "w") as f:
        f.write(contents)
        f.close()
