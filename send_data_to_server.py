import os
import requests
import json
import subprocess
from dotenv import load_dotenv

load_dotenv()
__tmp_data = []
API_URL = os.getenv("API_URL")
API_TOKEN = os.getenv("API_TOKEN")
RAM_DIR = os.getenv("RAM_DIR")
CPU_INFO = os.getenv("CPU_INFO")
MACHINE_CODE = os.getenv("MACHINE_CODE")

for file in os.listdir(RAM_DIR):
    if file.endswith(".txt"):
        with open(os.path.join(RAM_DIR, file), "r") as f:
            contents = f.read()
            __tmp_data.append({"file": file, "contents": contents})

try:
    data = subprocess.run(CPU_INFO, shell=True, check=True, capture_output=True)
    MACHINE_CODE = str(data.stdout.decode("utf-8"))
except Exception as error:
    print("MACHINE_CODE error: ", error)

DATA = json.dumps({"data": __tmp_data})

HEADERS = {"Content-Type": "application/json", "cache-control": "no-cache"}

r = requests.post(
    API_URL + "/machines/sensor/" + MACHINE_CODE, headers=HEADERS, data=DATA
)
